from config import * 
from modelo import *

# Aqui vão:
import pygame
import random

pygame.init()

#Definir a tela 
max_x = 800
min_y = 600

screen = pygame.display.set_mode((max_x, min_y))
clock = pygame.time.Clock()

# definir as classes Jogador(nome,x e y, que é a posição dele, n_movimentos) e Labirinto(definicao_obstaculos: Texto));
class Jogador (pygame.sprite.Sprite):
    def __init__(self, nome, x, y):
        super().__init__()
        self.nome = nome
        self.image = pygame.Surface((25,25))
        self.image.fill ((235, 216, 52)) #Preenche a cor do quadrado
        self.rect = self.image.get_rect(topleft=(x,y)) # retângulo em volta da imagem
        self.n_movimentos = 0
        self.velocidade = 5

# definir a classe Obstaculo
class Obstaculo (pygame.sprite.Sprite):
    def __init__(self, x, y, b, h):
        super().__init__()
        self.image = pygame.Surface((b,h))
        self.rect = self.image.get_rect(topleft=(x,y)) # retângulo em volta da imagem
        self.obstaculo = int


#posição inicial do jogador
x = 50
y = 50

nome = input("Me diga qual é o seu nome:")

# Definir/criar o jogador (sugestão de nome da variável/objeto: player)
player = Jogador(nome, x, y)
player_group =pygame.sprite.Group()
player_group.add(player)

# Definir/criar o labirinto 
# será uma lista de obstáculos

paredes = []
paredes_group = pygame.sprite.Group()
mapa = [
    "WWWWWWWWWWWWWWWWWWWW",
    "W                  W",
    "W         WWWWWW   W",
    "W   WWWW       W   W",
    "W   W        WWWW  W",
    "W WWW  WWWW        W",
    "W   W     W W      W",
    "W   W     W   WWW  W",
    "W   WWW WWW   W W  W",
    "W     W   W   W W  W",
    "WWW   W   WWWWW W  W",
    "W W      WW        W",
    "W W   WWWW   WWW   W",
    "W     W        W   W",
    "WWWWWWWWWWWEWWWWWWWW",
]

eixoX = 0
eixoY = 0

for row in mapa:
    for col in row:
        if col == "W":
            paredes_group.add(Obstaculo(eixoX, eixoY, 30, 30))

        if col == "E":
            end_rect = pygame.Rect(eixoX, eixoY, 30, 30)
            finalX = eixoX
            finalY = eixoY
        eixoX += 30
    eixoY += 30
    eixoX = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit

    #Guarda a posição anterior do jogador
    x_anterior = player.rect.x
    y_anterior = player.rect.y

    # captura os eventos do teclado
    pk = pygame.key.get_pressed()

    # Teclas de movimento;
    if pk[pygame.K_LEFT]:
        player.rect.x -= player.velocidade
        player.n_movimentos += 1
    if pk[pygame.K_RIGHT]:
        player.rect.x += player.velocidade
        player.n_movimentos += 1
    if pk[pygame.K_UP]:
        player.rect.y -= player.velocidade
        player.n_movimentos += 1
    if pk[pygame.K_DOWN]:
        player.rect.y += player.velocidade
        player.n_movimentos += 1

    # Verificação de colisão: o jogador encostou em algum dos obstáculos?;
    quem_colidiu = pygame.sprite.spritecollide(player, paredes_group, False)
    
    # se houve colisão
    if len (quem_colidiu) > 0:
        player.rect.x = x_anterior
        player.rect.y = y_anterior
        player.n_movimentos -= 1

    #Vai verificar se chegou na saida
    print(player.rect.x, player.rect.y)
    if (player.rect.x == finalX) and (player.rect.y == finalY):
        break


    # vai desenhar os objetos (rect): desenhar o jogador e desenhar os obstáculos
    screen.fill("red")
    player_group.draw(screen)
    paredes_group.draw(screen)

    pygame.display.flip()
    clock.tick(60)

    
# Aqui criaremos um objeto da classe Resultado que será gravado no banco;
x = Resultado(nome_jogador = player.nome, player_movimentos = player.n_movimentos)

db.session.add(x)
db.session.commit()